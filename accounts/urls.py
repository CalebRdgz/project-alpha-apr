from django.urls import path
from accounts.views import signup_form
from django.contrib.auth.views import LoginView, LogoutView


urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("signup/", signup_form, name="signup"),
]
